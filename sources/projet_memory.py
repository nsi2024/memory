########## Importer les modules necessaires ##############

'''

on importe les modules nécessaires au programme

'''

from tkinter import *

from tkinter.font import Font

from random import randrange

from random import shuffle



##########################################################

##########    Fonctions ##################################

##########################################################

def verif():

    global col, lig, Noclic, NoJoueur, prece_col, prece_lig, carte_1, L_carte_trouve, L_carte_trouve_pos, placement_fleche

    '''

    fonction qui vérifie sur les deux cartes cliquées ont des listes identiques

    puis qui retourne les cartes si elles ne le sont pas

    si les cartes sont juste le joueur rejoue

    '''

    carte_2 = affichecarte(lig-1, col)

    if carte_1 not in L_carte_trouve and carte_2 not in L_carte_trouve:

        valeurclic1 = deck[carte_1]

        valeurclic2 = deck[carte_2]

        if valeurclic1 != valeurclic2:

            dessine(lig-1, col, carte_2)

            NoJoueur += 1

            if NoJoueur == 4:

                NoJoueur = 0

            placement_fleche += 4

            if placement_fleche == 18:

                placement_fleche = 2

            fleche(placement_fleche)

        else :

            a=200

            i=0

            while i<len(L):

                b=100

                j=0

                while j<len(L[0]):

                    j+=1

                    Canevas.create_rectangle(b,a,b+75,a+75,fill='#AE1717')

                    b+=100

                a+=100

                i+=1

            points()

            L_carte_trouve.append(carte_1)

            pos = (lig-1)*8+col

            pos_2 = (prece_lig-1)*8+prece_col

            L_carte_trouve_pos.append((L_carte[pos_2][0], L_carte[pos_2][1], L_carte[pos_2][2], L_carte[pos_2][3]))

            L_carte_trouve_pos.append((L_carte[pos][0], L_carte[pos][1], L_carte[pos][2], L_carte[pos][3]))

            for i in range(len(L_carte_trouve_pos)):

                    Canevas.create_rectangle(L_carte_trouve_pos[i][0], L_carte_trouve_pos[i][1], L_carte_trouve_pos[i][2], L_carte_trouve_pos[i][3], fill = 'white')

            gagner()

        Noclic = 0



def clic(event):

    global Noclic, lig, col, prece_col, prece_lig, carte_1, L_carte_trouve, L_carte_trouve_pos

    '''

    fonction qui retourne la carte quand on clique dessus

    on doit pouvoir en retourner deux à la suite

    si la personne trouve les mêmes elle doit pouvoir recommencer

    il faut bien cliquer sur la carte et pas à côté

    '''

    X=event.x

    Y=event.y

    for i in range(len(L_carte)):

        if X >= L_carte[i][0] and Y >= L_carte[i][1] and X <= L_carte[i][2] and Y <= L_carte[i][3] and Noclic<2:

            col=(X-100)//100

            lig=(Y-100)//100

            if (col != prece_col or lig != prece_lig) and Noclic == 0:

                Noclic += 1

                prece_col = col

                prece_lig = lig

                a=200

                i=0

                while i<len(L):

                    b=100

                    j=0

                    while j<len(L[0]):

                        j+=1

                        Canevas.create_rectangle(b,a,b+75,a+75,fill='#AE1717')

                        b+=100

                    a+=100

                    i+=1

                for i in range(len(L_carte_trouve_pos)):

                    Canevas.create_rectangle(L_carte_trouve_pos[i][0], L_carte_trouve_pos[i][1], L_carte_trouve_pos[i][2], L_carte_trouve_pos[i][3], fill = 'white')

                carte_1 = affichecarte(lig-1, col)

                dessine(lig-1, col, carte_1)



def fleche(i):

    '''

    fonction qui fait bouger la fleche selon le joueur qui joue

    '''

    global obj1, obj2

    Canevas.coords(obj1,10+i*50,70,40+i*50,50)

    Canevas.coords(obj2,40+i*50,40,70+i*50,60,40+i*50,80)



def affichecarte(i,j):

    '''

    trouver le numero de la carte dans la liste L

    ex : affichecarte(2,3) -> 13

    '''

    return(L[i][j])



def dessine(i,j,k):

    '''

    dessine les cartes i,j,k du deck

    (donc par rapport à la liste coul car on ne sait pas si on la fait)

    dessine(2,3,13) -> on prend deck[13]=[1, 0, 1]

    on doit dessine a l'endroit 2,3 la carte bleu, carre, 2

    '''

    pos = i*8+j

    if deck[k][0] == 0:

        couleur = 'red'

    else:

        couleur = 'blue'

    if deck[k][1] == 0:

        Canevas.create_rectangle(L_carte[pos][0], L_carte[pos][1], L_carte[pos][2], L_carte[pos][3], fill=couleur)

    if deck[k][1] == 1:

        Canevas.create_polygon((L_carte[pos][0]+L_carte[pos][2])/2, L_carte[pos][1], L_carte[pos][0], L_carte[pos][3], L_carte[pos][2], L_carte[pos][3], fill=couleur)

    if deck[k][1] == 2:

        Canevas.create_oval(((L_carte[pos][0]+L_carte[pos][2])/2)+37.5, ((L_carte[pos][1]+L_carte[pos][3])/2)+37.5, ((L_carte[pos][0]+L_carte[pos][2])/2)-37.5, ((L_carte[pos][1]+L_carte[pos][3])/2)-37.5, fill=couleur)

    Canevas.create_text((L_carte[pos][0]+L_carte[pos][2])/2,(L_carte[pos][1]+L_carte[pos][3])/2,text=str(deck[k][2]+1),fill="black",font=Mapolice)



def points():

    '''

    gere le score des joueurs

    '''

    global Score

    Score[NoJoueur] += 2

    Canevas.create_rectangle(150,80,250,120, fill='white')

    Canevas.create_rectangle(350,80,450,120, fill='white')

    Canevas.create_rectangle(550,80,650,120, fill='white')

    Canevas.create_rectangle(750,80,850,120, fill='white')

    affscore=[]

    for i in range(4):

        affscore.append(Canevas.create_text(200+200*i,100,text=Score[i],fill="black",font=Mapolice))

    for i in range(len(affscore)):

        Canevas.itemconfig(affscore[i],text=Score[i])



def gagner():

    '''

    ouvre une page pour annoncer le gagnant de la partie

    bouton qui ferme toutes les pages

    '''

    if len(L_carte_trouve) == 24:

        player = 'J1'

        meilleur_score = Score[0]

        for i in range(1, len(Score)):

            if Score[i] > meilleur_score:

                meilleur_score = Score[i]

                player = 'J'+str(i+1)

        global Fenetre

        Fenetre = Tk()

        Fenetre.title("Gagner")

        Canevas = Canvas(Fenetre,width=300,height=100,bg ='white')

        Canevas.pack()

        police = Font(family='Comics sans MS', size=25)

        Canevas.create_text(150,32,text= "Bravo "+ player + " vous avez gagné",fill="black",font=police)



        bouton1=Button(Fenetre, text="Fermer", command= fermer)

        bouton1.place(x=125,y=75)



        Canevas.bind('<Button-1>',clic)

        Fenetre.mainloop()



def fermer():

    '''

    ferme toutes les fenetres

    '''

    global Fenetre, Mafenetre

    Fenetre.destroy()

    Mafenetre.destroy()

##########################################################

##########    Variables ##################################

##########################################################

LARGEUR_ECRAN=1000

NB_LINES=6

NB_COLS=8



'''

liste qui correspond aux cartes du jeu

'''

L=[[0,0,1,1,2,2,3,3],

   [4,4,5,5,6,6,7,7],

   [8,8,9,9,10,10,11,11],

   [12,12,13,13,14,14,15,15],

   [16,16,17,17,18,18,19,19],

   [20,20,21,21,22,22,23,23]]



'''

coul=['red','blue',]

carte=[coul,symbol, num]

0=rouge,1=bleu

0=carre,1=triangle, 2=rond

0=1,1=2,2=3,3=4

 ex : [1,1,3] : bleu, triange, num=3+1=4

 ex : [0,0,2] : rouge, carre, num=2+1=3

2 couleurs, 3 symboles, 4 chiffres

'''

deck=[]

'''

listes avec les couleurs, symboles, chiffres pour qui les cartes

soit toutes différentes sans faire 1 fonction par carte

'''

for i in range(2):

    for j in range(3):

        for k in range(4):

            deck.append([i,j,k])

shuffle(deck)





valeurclic1=0

valeurclic2=0

'''

valeur de la carte lors des deux clic disponiblent du joueur

'''

prece_lig = 0

prece_col = 0

'''

valeur des precedentes colonne et ligne obtenue lors du precedent clic

'''

L_carte_trouve = []

L_carte_trouve_pos = []

'''

liste des cartes et des positions des cartes déjà trouvées par l'un des joueurs

'''

Noclic=0

'''

nombre de fois cliqué sur une carte

'''



Score=[0,0,0,0]

'''

variable pour chaque joueurs qui s'inisialise à 0 en début de partie

on ajoute +2 quand une paire est trouvée

'''



placement_fleche = 2

'''

variable afin de gérer l'emplacement de la flèche

'''



NoJoueur=0

#########################################################

########## Interface graphique ##########################

##########################################################

'''

pour afficher l'interface graphique

'''

Mafenetre = Tk()

Mafenetre.title("Titre")

Canevas = Canvas(Mafenetre,width=1000,height=1000,bg ='white')

Canevas.pack()

Mapolice = Font(family='Liberation Serif', size=20)

# création d'une police pour l'affichage du texte

'''

programme qui fait la grille des cartes

'''

L_carte=[]

a=200

i=0

while i<len(L):

    b=100

    j=0

    while j<len(L[0]):

        j+=1

        Canevas.create_rectangle(b,a,b+75,a+75,fill='#AE1717')

        L_carte.append((b,a,b+75,a+75))

        b+=100

    a+=100

    i+=1



'''

interface graphique des joueurs + de la flèche

'''

Canevas.create_text(200,50,text="J1",fill="black",font=Mapolice)

Canevas.create_text(400,50,text="J2",fill="light pink",font=Mapolice)

Canevas.create_text(600,50,text="J3",fill="#5c4cbf",font=Mapolice)

Canevas.create_text(800,50,text="J4",fill="light blue",font=Mapolice)

obj1=Canevas.create_rectangle(10,70,40,50, fill='#AE1717')

obj2=Canevas.create_polygon(40,40,70,60,40,80, fill='#AE1717')

Canevas.coords(obj1,10+placement_fleche*50,70,40+placement_fleche*50,50)

Canevas.coords(obj2,40+placement_fleche*50,40,70+placement_fleche*50,60,40+placement_fleche*50,80)

Canevas.create_rectangle(150,80,250,120, fill='white')

Canevas.create_rectangle(350,80,450,120, fill='white')

Canevas.create_rectangle(550,80,650,120, fill='white')

Canevas.create_rectangle(750,80,850,120, fill='white')



affscore=[]

for i in range(4):

    affscore.append(Canevas.create_text(200+200*i,100,text=Score[i],fill="black",font=Mapolice))

Score=[0,0,0,0]

for i in range(len(affscore)):

    Canevas.itemconfig(affscore[i],text=Score[i])

'''

bouton nécessaire à la fonction vérifier

'''

bouton1=Button(Mafenetre, text="   vérifier    ", command=lambda : verif())

bouton1.place(x=900,y=200)

###########################################################

########### Receptionnaire d'évènement ####################

###########################################################

Canevas.bind('<Button-1>',clic)

##########################################################

############# Programme principal ########################

##########################################################

shuffle(L[0])

shuffle(L[1])

shuffle(L[2])

shuffle(L[3])

shuffle(L[4])

shuffle(L[5])

shuffle(L)



'''

for line in range(NB_LINES):

    for col in range(NB_COLS):

        Canevas.create_image(X0+col*SIDE, Y0+line*SIDE, image=shuffle)

'''



###################### FIN ###############################

Mafenetre.mainloop()